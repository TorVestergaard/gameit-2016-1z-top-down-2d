using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class Idle : AnimationState
{
    public override void Update()
    {
        base.Update();

        if (true)
        {
            this.Boss.EnterState(this.Boss.SuperAttack);
        }
        else if (true)
        {
            this.Boss.EnterState(this.Boss.DefaultAttack);
        }
        else if (this.Boss.Health <= 0)
        {
            this.Boss.EnterState(this.Boss.DeathState);
        }
    }

    //public override BossState GetNextState()
    //{
    //}
}