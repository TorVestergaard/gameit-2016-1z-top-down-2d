using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Boss))]
[Serializable]
public abstract class BossState : MonoBehaviour
{
    public Boss Boss { get; private set; }

    private void Start()
    {
        this.Boss = this.GetComponent<Boss>();
    }

    public virtual void Enter()
    {
    }

    public virtual void Exit()
    {
    }

    public virtual void Update()
    {
    }

    //public abstract BossState GetNextState();
}