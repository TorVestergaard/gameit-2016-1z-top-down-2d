using System;
using System.Collections.Generic;
using System.Linq;

public interface IDamageAble
{
    float Health { get; }

    void Damage(float amount);
}