using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class SingleAnimationState : AnimationState
{
    [SerializeField]
    private BossState nextState;

    //private bool isDone;

    //public override BossState GetNextState()
    //{
    //    if (this.isDone)
    //    {
    //        return this.nextState;
    //    }

    //    return null;
    //}

    protected override void OnAnimationComplete()
    {
        //this.isDone = true;
        this.Boss.EnterState(this.nextState);
    }
}