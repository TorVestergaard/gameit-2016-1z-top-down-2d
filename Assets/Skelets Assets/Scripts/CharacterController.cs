﻿using System;
using System.Collections;
using UnityEngine;

//public class CharacterController : MonoBehaviour
//{
//    [SerializeField]
//    private CharacterMotor characterMotor;

//    [SerializeField]
//    private InputManager inputManager;

//    [SerializeField]
//    private WeaponManager weaponManager;

//    private void Update()
//    {
//        if (this.inputManager.ShouldFire())
//        {
//        }
//    }
//}

//public class WeaponManager
//{
//}

//public abstract class InputManager
//{
//    public abstract Vector2 GetMoveDirection();

//    public abstract bool ShouldFire();
//}

//public abstract class PlayerState : State
//{
//    [SerializeField]
//    private SpriteRenderer renderer;

//    [SerializeField]
//    private Sprite[] spriteSheet;

//    [SerializeField]
//    private float animationSpeed = 2f;

//    private float currentAnimation = 0f;

//    public override State StateUpdate()
//    {
//        this.currentAnimation += Time.deltaTime * animationSpeed;
//        this.currentAnimation = Mathf.Repeat(this.currentAnimation, 1f);
//        this.renderer.sprite = this.spriteSheet[Mathf.RoundToInt(this.currentAnimation * this.spriteSheet.Length)];

//        return null;
//    }
//}

//public class PlayerIdle : PlayerState
//{
//    public override void Enter()
//    {
//    }

//    public override void Exit()
//    {
//    }

//    public override State GetNextState()
//    {
//        if (Input.GetAxis("Horizontal") != 0f)
//        {
//            return this.GetComponent<PlayerRun>();
//        }

//        return null;
//    }
//}

//public class CharacterMotor : MonoBehaviour
//{
//    public bool Grounded { get; private set; }

//    public void SetVelocity(Vector2 amount)
//    {
//    }

//    public void Move(Vector2 amount)
//    {
//    }
//}

//public class PlayerRun : PlayerState
//{
//    [SerializeField]
//    private Transform transform;

//    [SerializeField]
//    private float speed = 5f;

//    public override void Enter()
//    {
//    }

//    public override void Exit()
//    {
//    }

//    public override void StateUpdate()
//    {
//    }

//    public override State GetNextState()
//    {
//        throw new NotImplementedException();
//    }
//}

//public abstract class PlayerJump
//{
//}

//public abstract class State : MonoBehaviour
//{
//    public virtual void Enter()
//    {
//    }

//    public virtual void Exit()
//    {
//    }

//    public abstract State GetNextState();

//    public virtual void StateUpdate()
//    {
//    }
//}