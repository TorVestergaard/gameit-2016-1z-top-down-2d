using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public abstract class AnimationState : BossState
{
    [SerializeField]
    private SpriteRenderer renderer;

    [SerializeField]
    private Sprite[] spriteSheet;

    [SerializeField]
    private float animationSpeed = 2f;

    private float currentAnimation = 0f;

    protected float AnimationSpeed { get { return this.animationSpeed; } }

    public override void Update()
    {
        this.currentAnimation += Time.deltaTime * animationSpeed;

        if (this.currentAnimation >= 1f)
        {
            this.OnAnimationComplete();
        }

        this.currentAnimation = Mathf.Repeat(this.currentAnimation, 1f);
        this.renderer.sprite = this.spriteSheet[Mathf.RoundToInt(this.currentAnimation * this.spriteSheet.Length)];
    }

    protected virtual void OnAnimationComplete()
    {
    }
}