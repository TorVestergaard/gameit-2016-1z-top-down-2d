using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Boss : MonoBehaviour, IDamageAble
{
    [SerializeField]
    private Idle idleState;

    [SerializeField]
    private SingleAnimationState superAttack;

    internal void EnterState(BossState nextState)
    {
        this.currentState.Exit();
        this.currentState = nextState;
        this.currentState.Enter();
    }

    [SerializeField]
    private SingleAnimationState defaultAttack;

    [SerializeField]
    private DeathState deathState;

    public Idle IdleState { get { return this.idleState; } }
    public SingleAnimationState SuperAttack { get { return this.superAttack; } }
    public SingleAnimationState DefaultAttack { get { return this.defaultAttack; } }
    public DeathState DeathState { get { return this.deathState; } }

    public float Health { get; private set; }

    private BossState currentState;

    private void Update()
    {
        this.currentState.Update();

        //BossState nextState = this.currentState.GetNextState();

        //if (nextState != null)
        //{
        //    this.currentState.Exit();
        //    this.currentState = nextState;
        //    this.currentState.Enter();
        //}
    }

    public void Damage(float amount)
    {
        this.Health -= amount;
    }
}